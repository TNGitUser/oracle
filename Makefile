# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/28 15:42:43 by lucmarti          #+#    #+#              #
#    Updated: 2019/04/23 10:43:13 by lucmarti         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

Red=\033[31m
Green=\033[32m
Cyan=\033[36m
End=\033[0m

TARGET = oracle

SRC_PATH = ./srcs
SRC_FILE = main.c gnl.c

OBJ_PATH = ./objs
OBJ_FILE = $(SRC_FILE:.c=.o)
DEP_FILE = $(SRC_FILE:.c=.d)

SRC = $(addprefix $(SRC_PATH)/,$(SRC_FILE))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_FILE))
DEP = $(addprefix $(OBJ_PATH)/,$(DEP_FILE))

CFLAGS = -Wall -Wextra -g -I./includes
CPPFLAGS = -MMD -MP
LIB = libs/libft.a

ifndef VERBOSE
.SILENT:
VERBOSE=0
endif

all: $(TARGET)

$(TARGET): $(OBJ) 
	@$(MAKE) -C libft/ VERBOSE=$(VERBOSE)
	@gcc $(CFLAGS) -o $(TARGET) $(OBJ) $(LIB)
	@echo "Compilation of $(Cyan)$(TARGET)$(End)   :    ${Green}Done${End}"

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir $(OBJ_PATH) 2> /dev/null || true
	@gcc $(CFLAGS) -c $< $(CPPFLAGS) -o $@
	@echo "[${Green}Compiled${End}] : $<"

.PHONY: clean fclean re

clean:
	@rm -f $(OBJ)
	@$(MAKE) -C libft/ clean
	@rm -f $(DEP)
	@rmdir $(OBJ_PATH) 2> /dev/null || (true && if [ -d "$(OBJ_PATH)" ]; then\
		echo "$(Red)ERROR$(End)	: $(OBJ_PATH) is not empty."; fi)
	@echo "$(Red)ft_ls$(End) : Removing objects"

fclean: clean
	@rm -f $(TARGET)
	@$(MAKE) -C libft/ clean
	@echo "$(Red)ft_ls$(End) : Removing $(TARGET)"

re: fclean all

-include $(DEP)
