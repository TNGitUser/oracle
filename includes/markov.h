/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   markov.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 12:33:32 by lucmarti          #+#    #+#             */
/*   Updated: 2019/04/15 11:40:07 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MARKOV_H
# define MARKOV_H

/*
**	t_suffix	:	Structure is used to store the suffix following the prefix
**	├-> suffix	: character following the prefix (ex : 'a')
**	├-> next	: next suffix like a chained list
**	└-> prob	: probability (< 1) of the prefix being followed by the current
**				suffix
*/

typedef struct			s_suffix
{
	char						*suffix;
	struct s_suffix	*next;
	long double			prob;
	long						count;
}						t_suffix;

/*
**	t_prefix	:	Structure is used to store the prefix of a chain
**	├-> prefix	: character starting a chain (ex : 'a')
**	├-> suffix	: list of char following the prefix (ex : 'a -> b')
**	└-> next	: next prefix like a chained list
*/

typedef	struct			s_prefix
{
	char				*prefix;
	t_suffix			*suffix;
	long						count;
	struct s_preffix	*next;
}						t_prefix;

/*
**	t_markov	:	Structure is used to store the prefix of a chain
**	├-> order	: size of the prefix (ex : order 0 - '' ; order 1 - 'a')
**	└-> prefix	: character starting a chain (ex : 'a')
**
**	The prefix list will be of x entry each having a list of suffix.
**	That list of suffix will have a total probability of 1.
*/

typedef	struct			s_markov
{
	int					order;
	t_prefix			*prefix;
}						t_markov;

#endif
