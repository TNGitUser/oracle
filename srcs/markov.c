/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   markov.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/15 11:14:40 by lucmarti          #+#    #+#             */
/*   Updated: 2019/04/15 11:22:07 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "oracle.h"

t_markov	*markov_init(int order)
{
	t_markov	*chain;

	chain = NULL;
	if (order < 0 || order > 9 || !(chain = malloc(sizeof(t_markov))))
		return (NULL);
	chain->order = 0;
}
