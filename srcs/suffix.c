/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   suffix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/15 11:22:16 by lucmarti          #+#    #+#             */
/*   Updated: 2019/04/15 11:31:34 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "oracle.h"

void		suffix_free(t_suffix *snode)
{
	if (!snode)
		return ;
	if (snode->next)
		ft_memdel((void **)&(snode->next));
	if (snode->suffix)
		ft_memdel((void **)&(snode->suffix));
}

t_suffix	*suffix_init(char *suffix, t_suffix *next, long double prob)
{
	t_suffix *snode;

	node = NULL;
	if (!(snode = malloc(sizeof(t_suffix))))
		return (NULL);
	snode->suffix = suffix != NULL ? suffix : NULL;
	snode->next = next != NULL ? next : NULL;
	snode->prob = prob != -1 ? prob : -1;
	return (snode);
}
