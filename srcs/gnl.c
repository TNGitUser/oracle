/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 15:13:20 by lucmarti          #+#    #+#             */
/*   Updated: 2019/04/15 11:13:46 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "oracle.h"

static int		ft_checkinit(t_list **alst, const int fd, char **line)
{
	char	buf[1];

	if (fd < 0 || !line || (read(fd, buf, 0) == -1))
		return (0);
	if (!(*alst))
	{
		*alst = ft_lstnew(NULL, BUFF_SIZE);
		if (!(*alst))
			return (0);
		(*alst)->content_size = fd;
	}
	return (1);
}

static int		ft_setlist(char *str, t_list **alst, const int fd)
{
	t_list	*lst;
	size_t	i;
	char	*tmp;

	while (*str)
	{
		i = ft_strchr(str, '\n') ? ft_strchr(str, '\n') - str : ft_strlen(str);
		tmp = ft_strsub(str, 0, i);
		if (!(lst = ft_lstnew(tmp, fd)))
			return (0);
		free(tmp);
		ft_lstadd_back(alst, lst);
		if (ft_strlen(str) != i)
		{
			tmp = str;
			str = ft_strsub(str, i + 1, ft_strlen(str) - (i + 1));
			free(tmp);
		}
		else
			break ;
	}
	free(str);
	return (1);
}

static int		ft_readfile(const int fd, t_list **alst)
{
	char	*str;
	char	*tmp;
	char	buf[BUFF_SIZE + 1];
	size_t	bsize;

	str = ft_strdup("");
	bsize = 0;
	while (bsize < BUFF_SIZE + 1)
		buf[bsize++] = 0;
	while (read(fd, buf, BUFF_SIZE) > 0 && ((bsize = 0) == 0))
	{
		tmp = str;
		if (!(str = ft_strjoin(str, buf)))
			return (0);
		ft_memdel((void **)&tmp);
		while (bsize < BUFF_SIZE + 1)
			buf[bsize++] = 0;
	}
	if (*str != '\0')
		ft_setlist(str, alst, fd);
	else
		ft_memdel((void **)&str);
	return (1);
}

static t_list	*ft_getfd(t_list **alst, t_list **prev, size_t fd)
{
	t_list	*cur;

	if (*alst == NULL)
		return (NULL);
	cur = *alst;
	while (cur->content_size != fd)
	{
		*prev = cur;
		cur = cur->next;
	}
	return (cur);
}

int				get_next_line(const int fd, char **line)
{
	static t_list	*head;
	t_list			*prev;
	t_list			*nxt;

	if (!ft_checkinit(&head, fd, line) || !ft_readfile(fd, &head))
		return (-1);
	nxt = head->next;
	prev = head;
	if (!nxt)
		*line = ft_strdup("");
	while (nxt)
	{
		if (nxt->content != NULL && nxt->content_size == (size_t)fd)
		{
			*line = ft_strdup(nxt->content);
			prev->next = nxt->next;
			free(nxt->content);
			free(nxt);
			break ;
		}
		nxt = ft_getfd(&head, &prev, (size_t)fd);
	}
	return (!nxt ? 0 : 1);
}
