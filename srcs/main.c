/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 12:23:28 by lucmarti          #+#    #+#             */
/*   Updated: 2019/04/15 15:55:44 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "oracle.h"
#include <stdio.h>
#include <limits.h>

char *get_part(char *str, int order)
{
}

void set_prefix(t_markov *chain, char *prefix)
{
		
}

/*
**	TODO : Parsing functions to have propers options
*/

int	main(int ac, char **av)
{
	char		*line;
	int			fd;
	int			ret;
	t_markov	*chain;

	printf("Oracle begins.\n");
	if (ac <= 1)
	{
		printf("usage : ./oracle [name_set] [order (0-9)]\n");
		exit(1);
	}
	ret = 0;
	if ((fd = open(av[1], O_RDONLY)) < 0 || fd > OPEN_MAX)
		exit(1);
	line = NULL;
	while ((ret = get_next_line(fd, &line)) > 0)
	{
		printf("%s\n", line);
		ft_memdel((void **)&line);
	}
	ft_memdel((void **)&line);
	return (0);
}
