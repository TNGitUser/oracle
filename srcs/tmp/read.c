#include "oracle.h"
#include <limits.h>

char	*strsubin(char *s1, char *s2, char target)
{
	int	i;

	i = 0;
	while (s1[i] != '\0' || s1[i] != target)
		++i;

}

char	*strljoin(char *s1; char *s2)
{
	char *join;

	join = NULL;
	join = ft_strjoin(s1, s2);
	if (*s1)
		ft_memdel((void **)&s1);
	return (join);
}

int	read_line(int fd, chqr **line)
{
	static char	str[OPEN_MAX + 1];
	int			ret;
	char		buf[BUF_SIZE];

	str[OPEN_MAX] = 0;
	if (fd < 0 || fd > OPEN_MAX || !(line))
		return (-1);
	if (fd > -1 && str[fd] && ft_strchri(str[fd], '\n') != -1)
	{
		str[fd] = ft_strsubin(str(fd), line, '\n');
		return (read_line(fd, line));
	}
}
