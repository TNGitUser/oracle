/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prefix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucmarti <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/15 11:31:39 by lucmarti          #+#    #+#             */
/*   Updated: 2019/04/15 11:40:08 by lucmarti         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "oracle.h"

void		prefix_free(t_prefix *pnode)
{
	if (!pnode)
		return ;
	if (pnode->prefix)
		ft_memdel((void **)&(pnode->prefix));
	if (pnode->suffix)
		free(void);	
	if (pnode->next)
		ft_memdel((void **)&(pnode->next));
}

t_prefix	*prefix_init(char *prefix, t_suffix *snode, t_prefix *next)
{
	t_prefix	*pnode;

	pnode = 0;
	if (!(pnode = malloc(sizeof(t_prefix))))
		return (NULL);
	pnode->prefix = prefix != NULL ? prefix : NULL;
	pnode->suffix = snode != NULL ? snode : NULL;
	pnode->next = next != NULL ? next : NULL;
	return (pnode);
}
